SRC=src
INC=include
OUT=bin
EXEC_NAME=teapong
LIB=libs

FILES = $(wildcard src/*.cpp , src/*.c)
TEMP = $(FILES:.c=.o) 
OBJECTS = $(TEMP:.cpp=.o)
DEPS = $(OBJECTS:.o=.d)

CXX=g++
CC=gcc

CFLAGS=-std=c11 -MMD -O3 -I$(INC)
CXXFLAGS=-std=c++17 -MMD -O3 -I$(INC)


ifeq ($(OS),Windows_NT)
	LIBS=-lglfw -lassimp -lirrklang
else
	UNAME_S := $(shell uname -s)

	ifeq ($(UNAME_S),Linux)
		LFLAGS=-L$(LIB)/linux
		LIBS=-lglfw -lGL -lassimp -ldl -pthread $(LIB)/linux/libIrrKlang.so

	endif

	ifeq ($(UNAME_S),Darwin)
		LIBS=-lglfw -lassimp -lirrklang
		LFLAGS=-L/usr/local/lib
	endif
endif

all: $(EXEC_NAME)

-include $(DEPS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(EXEC_NAME): $(OBJECTS)
	$(CXX) $(LFLAGS) $^ $(LIBS) -o $@

.PHONY: all clean

clean:
	rm -f $(OBJECTS) $(DEPS) $(EXEC_NAME)

